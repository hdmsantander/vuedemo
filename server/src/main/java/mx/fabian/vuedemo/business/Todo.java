package mx.fabian.vuedemo.business;

import lombok.*;

import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;

@Entity
@Data
@NoArgsConstructor
@Getter
@Setter
public class Todo {

	@Id
	@GeneratedValue
	private Long id;

	@NonNull
	private String title;

	@SuppressWarnings("unused")
	private Boolean completed = false;

}
