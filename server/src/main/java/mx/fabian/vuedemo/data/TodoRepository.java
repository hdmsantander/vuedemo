package mx.fabian.vuedemo.data;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

import mx.fabian.vuedemo.business.Todo;

@RepositoryRestResource
@Repository
public interface TodoRepository extends JpaRepository<Todo, Long> {
}
