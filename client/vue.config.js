module.exports = {
	runtimeCompiler: true,
	devServer: {
		proxy: 'http://demovue.fabiansantander.net/',
	},
	configureWebpack: {
		performance: {
			hints: false,
		},
		optimization: {
			splitChunks: {
				minSize: 10000,
				maxSize: 250000,
			},
		},
	},
};
