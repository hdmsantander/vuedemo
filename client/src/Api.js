import axios from 'axios';

// This url will map to a proxy url via nginx, for a local setup change to the URL and port configured in your spring app
const SERVER_URL = '/api';

const instance = axios.create({
	baseURL: SERVER_URL,
	timeout: 1000,
});

export default {
	// (C)reate
	createNew: (text, completed) =>
		instance.post('todos', { title: text, completed: completed }),
	// (R)ead
	getAll: () =>
		instance.get('todos', {
			transformResponse: [
				function(data) {
					return data ? JSON.parse(data)._embedded.todos : data;
				},
			],
		}),
	// (U)pdate
	updateForId: (id, text, completed) =>
		instance.put('todos/' + id, { title: text, completed: completed }),
	// (D)elete
	removeForId: id => instance.delete('todos/' + id),
};
