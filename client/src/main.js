/* eslint-disable no-unused-vars */
import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import Home from './components/Home.vue';
import Hobbies from './components/Hobbies.vue';
import Todos from './components/Todos.vue';
import VueLogger from 'vuejs-logger';
import VueNavigationBar from 'vue-navigation-bar';
import vueHeadful from 'vue-headful';
import 'vue-navigation-bar/dist/vue-navigation-bar.css';

Vue.config.productionTip = false;
Vue.use(VueLogger, options);
Vue.use(VueRouter);
Vue.component('vue-navigation-bar', VueNavigationBar);
Vue.component('vue-headful', vueHeadful);

const options = {
	isEnabled: true,
	logLevel: 'debug',
	stringifyArguments: false,
	showLogLevel: true,
	showMethodName: false,
	separator: '|',
	showConsoleColors: true,
};

const myRoutes = [
	{ path: '/todos', component: Todos },
	{ path: '/hobbies', component: Hobbies },
	{ path: '/', component: Home },
];

const myRouter = new VueRouter({
	routes: myRoutes,
	mode: 'history',
});

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router: myRouter,
	render: h => h(App),
});
