# Vue.js and Spring Boot demo
## An example of a single page web app

This repository contains the code needed to run a single page web app using docker containers. The app consists of a Javascript app made in Vue.js, served by Nginx and a back end for the REST api, that's implemented with Java using Spring Boot web services.

The app can be started though the [docker-compose.yml](https://gitlab.com/hdmsantander/vuedemo/-/blob/master/docker-compose.yml) file using **docker-compose** or building the containers manually to fit your needs.

* Dockerfile for the [server](https://gitlab.com/hdmsantander/vuedemo/-/blob/master/server/Dockerfile).
* Dockerfile for the [client](https://gitlab.com/hdmsantander/vuedemo/-/blob/master/client/Dockerfile).

Once started the back end also exposes a swagger ui page that documents the methods accepted by the api. The page exposed is in the format: **http://localhost/api/swagger-ui.html** adjusting the URL to the one configured.

The app is inspired in part by the following works: 

* Basic REST app using Vue.js and Spring Boot by [Andrew Hughes](https://github.com/moksamedia/okta-spring-boot-vue-crud-example).
* Vue.js navigation bar by [John Datserakis](https://github.com/johndatserakis/vue-navigation-bar).

You can see an example of the working app [here](http://demovue.fabiansantander.net/).
